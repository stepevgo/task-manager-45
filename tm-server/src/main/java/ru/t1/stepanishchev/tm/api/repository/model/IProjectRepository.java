package ru.t1.stepanishchev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull String sort);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    void clear();

    void clear(@NotNull String userId);

}